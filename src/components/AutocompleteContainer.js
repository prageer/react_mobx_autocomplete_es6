import React from 'react'
import { observer } from 'mobx-react'
import Autocomplete from './Autocomplete'
import CountryStore from '../stores/CountryStore'

 
const AutocompleteContainer = () => (
	<div>
      <div style={{marginBottom:"20px"}}>
        Hi, <b>Crowdbotics,</b> 
        <div>This is a sample React Autocomplete component developed using by create-react-app, mobx.</div>
        <br />
        <div>The server api is https://restcountries.eu</div>
        <br />
        <div>(create-react-app, which is made by facebook team and has the best tools and configuration we can get, in terms of React.)</div>
      </div>
    	Selected Country: {CountryStore.selectedCountry.name}
     	<Autocomplete
       		fetching={CountryStore.fetchByCode.fetching || CountryStore.fetchByName.fetching}
       		fetch={CountryStore.fetchCountries}
       		onSelect={CountryStore.handleSelectCountry}
       		value={CountryStore.value}
       		results={CountryStore.countries.map(country => ({
         		label: country.name,
         		value: country.alpha2Code
       		}))}
     	/>
   </div>
)
 
export default observer(AutocompleteContainer)